import React from "react";
import { render } from "react-dom";
import "./index.css";
import Beers from "./domains/beers";
import styled from "styled-components";

const Main = styled.main`
  flex: 1;
  display: flex;
  flex-direction: column;
  width: 100%;
  margin: 0 auto;
  margin-top: 2rem;
  max-width: 1024px;
  @media (max-width: 1024px) {
    margin-top: 0;
  }
  box-sizing: border-box;
`;

// A Router would be nice :')
render(
  <Main>
    <Beers />
  </Main>,
  document.getElementById("root")
);
