export const debounce = <T extends (...args: any[]) => any>(
  callback: T,
  waitFor: number
) => {
  let timeout: NodeJS.Timeout;
  let lock = false;
  return (...args: Parameters<T>): ReturnType<T> => {
    let result: any;
    clearTimeout(timeout);

    if (!lock) {
      lock = true;
      result = callback(...args);
    }
    timeout = setTimeout(() => {
      lock = false;
    }, waitFor);
    return result;
  };
};
