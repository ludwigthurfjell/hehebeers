import ky, { Options } from "ky";
import { IHttpOptions } from "./http.interfaces";

export default {
  post: function post<Type>(
    url: string,
    options?: IHttpOptions
  ): Promise<Type> {
    // Very opinionated header
    const kyoptions: Options = {
      headers: {
        "Content-Type": "application/json",
      },
    };
    if (options?.body) kyoptions.body = JSON.stringify(options.body);

    return ky.post(url, kyoptions).json<Type>();
  },
  get: function get<Type>(url: string, params?: {}): Promise<Type> {
    // Very opinionated header
    const kyoptions: Options = {
      headers: {
        "Content-Type": "application/json",
      },
    };

    if (Object.keys(params || {}).length) {
      url += `?${new URLSearchParams(params).toString()}`;
    }

    return ky.get(url, kyoptions).json<Type>();
  },
};
