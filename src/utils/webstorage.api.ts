export default {
  set: (key: string, val: any, session: boolean = false) => {
    try {
      if (session) {
        sessionStorage.setItem(key, JSON.stringify(val));
      } else {
        localStorage.setItem(key, JSON.stringify(val));
      }
    } catch {
      // TODO
    }
  },
  /**
   *
   * @param key
   * @param session Use sessionStorage, default false
   */
  get: (key: string, session: boolean = false) => {
    let item: any; // TODO

    if (session) {
      item = sessionStorage.getItem(key);
    } else {
      item = localStorage.getItem(key);
    }
    const isTrue = item === "true";
    const isFalse = item === "false";
    if (isTrue || isFalse) {
      item = isTrue ? true : false;
    } else {
      // Let's use try/catch here despite best practice opinions
      try {
        item = JSON.parse(item);
      } catch {}
    }
    return item;
  },
};
