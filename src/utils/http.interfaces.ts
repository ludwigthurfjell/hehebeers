export type HttpOptions = { 
  body?: { [key: string]: object | number | string };
}
