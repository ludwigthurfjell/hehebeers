import React from "react";

type Props = {
    value:string;
    onChange:(value:string) => void;
}
export default function SearchInput(props:Props){
    const {value, onChange} = props;
    return <input
    type="search"
    placeholder="Search"
    value={value}
    onChange={({target:{value}}) => onChange(value)}
  />
}