import React from "react";

type Props = {
  value: string;
  onChange: (value: string) => void;
  checked: boolean;
  label: string;
}

export default function RadioInput(props: Props) {
  const { value, onChange, checked, label } = props;
  const handleChange = ({
    target: { value },
  }: {
    target: { value: string };
  }) => {
    onChange(value);
  };
  return (
    <label>
      <input
        value={value}
        id={value}
        type="radio"
        onChange={handleChange}
        checked={Boolean(checked)}
      />
      {label}
    </label>
  );
}
