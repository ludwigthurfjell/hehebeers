import React, { useEffect, useState } from "react";
import RadioInput from "../../../layout/form/RadioInput";
import webstorageApi from "../../../utils/webstorage.api";
import { ABV, ALCOHOLICAL, NON_ALCOHOLICAL } from "../constants";
import { stringBoolToBool } from "../functions";
import { BeerProps, RandomBeerProps } from "./BeerFullInfo";

export default function RandomBeerForm(props: BeerProps & RandomBeerProps) {
  const { onGetRandom, onFormChanged, beer } = props;

  const [isAlcoholic, setIsAlcoholic] = useState<boolean>(function () {
    const cached = webstorageApi.get(ABV);
    return cached !== null ? stringBoolToBool(cached) : true;
  });

  useEffect(() => {
    if (!beer) {
      onGetRandom(isAlcoholic);
    }
  }, [beer, isAlcoholic]);

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    onGetRandom(isAlcoholic);
  };

  const handleRadioChange = (value: string) => {
    const isAlcoholic = value === ALCOHOLICAL;
    webstorageApi.set(ABV, isAlcoholic.toString());
    onFormChanged && onFormChanged();
    setIsAlcoholic(isAlcoholic);
  };
  return (
    <form onSubmit={handleSubmit} data-testid="b-random-form">
      <RadioInput
        label="Alcoholic"
        onChange={handleRadioChange}
        value={ALCOHOLICAL}
        checked={isAlcoholic}
      />
      <RadioInput
        label="Non-Alcoholic"
        onChange={handleRadioChange}
        value={NON_ALCOHOLICAL}
        checked={!isAlcoholic}
      />
      <button type="submit">Another beer</button>
    </form>
  );
}
