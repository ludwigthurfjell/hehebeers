import React, { FormEventHandler, useEffect, useRef, useState } from "react";
import styled from "styled-components";
import { PunkBeer } from "../beer.api";
import RandomBeerForm from "./RandomBeerForm";

export type BeerProps = {
  beer: PunkBeer;
};

export type RandomBeerProps = {
  onGetRandom: (isAlcoholic: boolean) => void;
  onFormChanged?: () => void;
};

const BeerImg = styled.img`
  height: 10rem;
  object-fit: scale-down;
  width: 100%;
  &:not([src]) {
    opacity: 0;
  }
`;

const ImgDiv = styled.div`
  display: flex;
  flex-direction: column;
  width: 10rem;
  align-items: center;
`;

const BeerSection = styled.section`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-evenly;
`;

const InfoContainer = styled.div`
  flex-basis: 70%;
`;

export default function Beer(props: BeerProps) {
  const { beer } = props;
  const [showImg, setShowImg] = useState(true);

  useEffect(() => {
    setShowImg(false);
  }, [beer.image_url]);

  return (
    <BeerSection>
      <ImgDiv>
        <h3>{beer.name}</h3>
        <BeerImg
          alt={beer.name}
          src={beer.image_url}
          onLoad={() => {
            setShowImg(true);
          }}
          crossOrigin="anonymous"
          hidden={!showImg}
        />
      </ImgDiv>
      <InfoContainer>
        <p>
          {beer.abv}% - {beer.tagline}
        </p>
        <p data-testid="b-random-desc">{beer.description}</p>

        <p>Great with</p>
        <ul>
          {(beer.food_pairing || []).map((s) => (
            <li key={s}>{s}</li>
          ))}
        </ul>
      </InfoContainer>
    </BeerSection>
  );
}

const RandomBeerArticle = styled.article`
  padding: 1rem;
  background: white;
  box-shadow: 0 1px 8px rgba(0, 0, 20, 0.15);
`;

Beer.Random = (props: BeerProps & RandomBeerProps) => {
  const [isChanging, setIsChanging] = useState(false);
  const handleGetRandom = (isAlcoholic: boolean) => {
    props.onGetRandom(isAlcoholic);
    setIsChanging(false);
  };

  const handleFormChanged = () => {
    setIsChanging(true);
  };
  return (
    <RandomBeerArticle data-testid="b-random-container">
      <h2>Random beverage! {isChanging && "*"}</h2>
      <hr />
      <Beer beer={props.beer} />
      <hr />
      <RandomBeerForm
        onGetRandom={handleGetRandom}
        beer={props.beer}
        onFormChanged={handleFormChanged}
      />
    </RandomBeerArticle>
  );
};
