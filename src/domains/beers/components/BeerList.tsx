import React from "react";
import styled from "styled-components";
import { PunkBeer } from "../beer.api";
import BeerListItem from "./BeerListItem";

const BeerUl = styled.ul`
  padding: 0;
  margin: 0;
`;

type Props = {
  beers: PunkBeer[];
}

export default function BeerList(props: Props) {
  const { beers = [] } = props;
  const anyBeers = beers.length > 0;
  return (
    <>
      {anyBeers && <h2>Beers</h2>}
      <BeerUl data-testid="b-list">
        {beers.map((beer) => (
          <BeerListItem key={`bl_${beer.id}`} {...beer} />
        ))}
      </BeerUl>
    </>
  );
}
