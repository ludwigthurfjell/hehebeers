import React, { useCallback, useState } from "react";
import styled from "styled-components";
import DateInput from "../../../layout/form/DateInput";
import RadioInput from "../../../layout/form/RadioInput";
import SearchInput from "../../../layout/form/SearchInput";
import { debounce } from "../../../utils/functions";
import webstorageApi from "../../../utils/webstorage.api";
import {
  SearchTypes,
  SEARCH_DATE_LT,
  SEARCH_NAME,
  SEARCH_TYPE,
} from "../constants";



type Props = {
  onSearch: (query: string, type: SearchTypes) => void;
};

export default function SearchPanel(props: Props) {
  const { onSearch } = props;
  const [nameSearch, setNameSearch] = useState<string>("");
  const [dateLessThanSearch, setDateLessThanSearch] = useState<string>(
    new Date().toISOString().substr(0, 10)
  );
  const [searchType, setSearchType] = useState(function () {
    const cached = webstorageApi.get(SEARCH_TYPE);
    return cached !== null ? cached : SEARCH_NAME;
  });

  // Let's prevent total spam!
  const handleSearch = useCallback(
    debounce((query, type) => onSearch && onSearch(query, type), 300),
    []
  );

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    const query = isNameSearch ? nameSearch : dateLessThanSearch;
    // Make a switch if we need more options :)
    const type = isNameSearch ? SearchTypes.Name : SearchTypes.DateLT;
    handleSearch(query.trim(), type);
  };

  const handleRadioChange = (value: string) => {
    webstorageApi.set(SEARCH_TYPE, value);
    setSearchType(value);
  };

  const handleNameInputChange = (value:string) => {
    if (/^([A-Za-z0-9-]+ ?)*$/.test(value)) {
      setNameSearch(value);
    }
  };

  const handleDateLessThanChange = (value:string)=>{
    setDateLessThanSearch(value)
  }

  const isNameSearch = searchType === SEARCH_NAME;
  const isDateLtSearch = searchType === SEARCH_DATE_LT;
  return (
    <>
      <h2>Search</h2>
      <form onSubmit={handleSubmit} data-testid="b-search-form">
        <RadioInput
          label="Name"
          checked={isNameSearch}
          onChange={handleRadioChange}
          value={SEARCH_NAME}
        />
        <RadioInput
          label="Earlier than date"
          checked={isDateLtSearch}
          onChange={handleRadioChange}
          value={SEARCH_DATE_LT}
        />
        {isDateLtSearch && (<DateInput value={dateLessThanSearch} onChange={handleDateLessThanChange} />
        )}
        {isNameSearch && (<SearchInput value={nameSearch} onChange={handleNameInputChange} />
        )}
        <button type="submit">Search</button>
      </form>
    </>
  );
}
