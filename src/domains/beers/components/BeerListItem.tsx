import React from "react";
import styled from "styled-components";
import { PunkBeer } from "../beer.api";

const ListItem = styled.li`
  box-shadow: 0 0 5px rgba(0, 0, 0, 0.15);
  padding: 0.5rem;
  padding-left: 1rem;
  margin: 0.5rem 0;
  height: 5rem;
  list-style: none;
`;

const TitleRow = styled.div`
  display: flex;
  align-items: baseline;
  h3 {
    margin: 0;
    margin-right: 0.6rem;
  }
  p {
    margin: 0;
    font-size: 14px;
  }
`;

const TextContent = styled.div`
  display: flex;
  flex-direction: column;
`;

const ListImg = styled.img`
  float: left;
  height: 5rem;
  margin-right: 1rem;
`;

const ShortedDesc = styled.p`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

export default function BeerListItem(props: PunkBeer) {
  const { description, tagline, image_url, name, abv } = props;
  return (
    <ListItem>
      <ListImg src={image_url} alt={name} crossOrigin="anonymous" />
      <TextContent>
        <TitleRow>
          <h3>{name}</h3>
          <p>
            <span>{abv}% - </span>
            {tagline}
          </p>
        </TitleRow>
        <ShortedDesc>{description}</ShortedDesc>
      </TextContent>
    </ListItem>
  );
}
