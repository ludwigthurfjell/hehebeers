import React, { useEffect, useState } from "react";

// Components
import BeerFullInfo from "./components/BeerFullInfo";
import BeerList from "./components/BeerList";
import SearchPanel from "./components/SearchPanel";

// Utils
import BeerApi, { PunkBeer } from "./beer.api";
import { ABV, SearchTypes } from "./constants";
import webstorageApi from "../../utils/webstorage.api";
import { stringBoolToBool } from "./functions";

export default function () {
  const [randomBeer, setRandomBeer] = useState<PunkBeer>();
  const [beers, setBeers] = useState<PunkBeer[]>([]);

  const handleGetRandom = async (isAlcoholic: boolean) => {
    const [random] = await BeerApi.getRandomBeer(isAlcoholic);
    setRandomBeer(random);
  };

  const handleSearch = async (query: string, type: SearchTypes) => {
    const beers = await BeerApi.getBeers(query, type);
    setBeers(beers);
  };

  useEffect(() => {
    const cached = webstorageApi.get(ABV);
    const isAlcoholic = cached !== null ? stringBoolToBool(cached) : true;
    handleGetRandom(isAlcoholic);
  }, []);

  return (
    <>
      {randomBeer && (
        <BeerFullInfo.Random onGetRandom={handleGetRandom} beer={randomBeer} />
      )}
      <SearchPanel onSearch={handleSearch} />
      <BeerList beers={beers} />
    </>
  );
}
