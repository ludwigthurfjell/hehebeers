/**
 * @returns "mm-yyyy" (1 indexed)
 */
export function dateToPunkDateStr(date: Date) {
  return `${date.getMonth() + 1}-${date.getFullYear()}`;
}

export function stringBoolToBool(str: string) {
  return str === "true" ? true : false;
}
