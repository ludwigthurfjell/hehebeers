//__tests__/functions.test.js
import {dateToPunkDateStr,stringBoolToBool} from "./functions";
describe("beer.functions", () => {

describe("dateToPunkDateStr", () => {

test("parse single digit month correct", () => {
    const expected = "1-2020"
    const actual = dateToPunkDateStr(new Date("2020-01-01"))
    expect(actual).toEqual(expected);
});
})

describe("stringBoolToBool", () => {
test("happy 'true' input", () => {
    const expected = true;
    const actual = stringBoolToBool("true");
    expect(actual).toEqual(expected);
})

test("happy 'false' input", () => {
    const expected = false;
    const actual = stringBoolToBool("false");
    expect(actual).toEqual(expected);
})

test("non 'bool' string input", () => {
    const expected = false;
    const actual = stringBoolToBool("hejsan 1337");
    expect(actual).toEqual(expected);
})
})
})
