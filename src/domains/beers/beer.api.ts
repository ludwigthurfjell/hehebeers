import httpApi from "../../utils/http.api";
import webstorageApi from "../../utils/webstorage.api";
import { BEERS_NON_ALCOHOLIC, SearchTypes } from "./constants";
import { dateToPunkDateStr } from "./functions";

const {
  urls: { punkApi },
} = require("../../../config.json");

export type PunkBeer = {
  id: number;
  name: string;
  tagline: string;
  first_brewed: string;
  description: string;
  image_url: string;
  abv: number;
  ubi: number;
  target_fg: number;
  food_pairing: string[];
};

export default {
  getRandomBeer: async function getRandomBeer(
    isAlcoholic = true
  ): Promise<PunkBeer[]> {
    let result: Promise<PunkBeer[]>;
    /**
     * No option to add parameters to random query,
     * thus we need to fetch all non alcoholic,
     * and since there were so few we can store in web storage.
     */
    if (!isAlcoholic) {
      const cached: PunkBeer[] = webstorageApi.get(BEERS_NON_ALCOHOLIC);
      if (cached) {
        // Lets "randomize" a little!
        const random = ~~(Math.random() * cached.length);
        result = new Promise((res) => res(cached.slice(random, random + 1)));
      } else {
        const res = httpApi.get<PunkBeer[]>(punkApi, { abv_lt: 1 });
        webstorageApi.set(BEERS_NON_ALCOHOLIC, await res);
        result = res;
      }
    } else {
      result = httpApi.get<PunkBeer[]>(punkApi + "/random");
    }
    return result;
  },

  getBeers: async function gatBeers(query: string = "", type: SearchTypes) {
    const urlParams = new URLSearchParams();
    switch (type) {
      case SearchTypes.DateLT:
        query &&
          urlParams.append("brewed_before", dateToPunkDateStr(new Date(query)));
        break;
      case SearchTypes.Name:
        query && urlParams.append("beer_name", query.replaceAll(" ", "_"));
        break;
    }
    const urlParamsStr = urlParams.toString() ? "?" + urlParams.toString() : "";
    return await httpApi.get<PunkBeer[]>(punkApi + urlParamsStr);
  },
};
