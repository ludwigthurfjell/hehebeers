export enum SearchTypes {
  DateLT = "SEARCH_DATE_LT",
  Name = "SEARCH_NAME",
}

export const ALCOHOLICAL = "alcoholical";
export const NON_ALCOHOLICAL = "nonalcoholical";
export const ABV = "abv";
export const SEARCH_TYPE = "searchtype";
export const SEARCH_DATE_LT = "searchdatelt";
export const SEARCH_NAME = "searchname";
export const BEERS_NON_ALCOHOLIC = "beers.nonalcoholic";
