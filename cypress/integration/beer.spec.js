describe("beers", () => {
  before(() => {
    cy.visit("http://localhost:1234");
    cy.clearLocalStorage();
  });

  it("should load live data", () => {
    cy.interceptRandomBeerRequest();
    cy.wait("@interceptRandomBeer");
    // Random container should have a name
    cy.getByTestId("b-random-container")
      .should("exist")
      .find("h3")
      .should(($p) => {
        expect($p.text()).to.match(/^(?!\s*$).+/);
      });

    // and a description
    cy.getByTestId("b-random-desc").should(($p) => {
      expect($p.text()).to.match(/^(?!\s*$).+/);
    });

    // Random form
    // Verify correct init radio
    cy.getByTestId("b-random-form")
      .should("exist")
      .find("[type='radio']#alcoholical")
      .should("be.checked");

    // Verify radio change with header notification
    cy.get("[type='radio']").check("nonalcoholical");
    cy.getByTestId("b-random-container").find("h2").should("contain", "*");

    // Get new random
    cy.getByTestId("b-random-form").submit();
    cy.interceptBeerRequest()
      .wait("@interceptBeer")
      .its("request.url")
      .should("include", "abv_lt=1");
    cy.getByTestId("b-random-container").find("h2").should("not.contain", "*");

    // Search
    // Name
    cy.getByTestId("b-search-form")
      .find("[type=search]")
      .first()
      .type("stout{enter}");

    cy.wait("@interceptBeer")
      .its("request.url")
      .should("include", "beer_name=stout");

    cy.getByTestId("b-list").children().should("exist");

    // Date less than
    cy.getByTestId("b-search-form").find("[type=radio]").last().click();
    cy.getByTestId("b-search-form").find("[type=date]").type("2018-12-31");

    cy.getByTestId("b-search-form").submit();

    cy.wait("@interceptBeer")
      .its("request.url")
      .should("include", "brewed_before=12-2018");
  });
});
