// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

// Cypress.Commands.add("replaceBeersRequest", () => {
//   cy.intercept(
//     {
//       method: "GET",
//       url: "/v2/beers/*",
//     },
//     { fixture: "randomBeer.json" }
//   );
//   cy.intercept(
//     {
//       method: "GET",
//       url: "/v2/beers*",
//     },
//     { fixture: "randomBeer.json" }
//   );
// });

Cypress.Commands.add("interceptBeerRequest", () => {
  cy.intercept({
    method: "GET",
    url: "/v2/beers*",
  }).as("interceptBeer");
});

Cypress.Commands.add("interceptRandomBeerRequest", () => {
  cy.intercept({
    method: "GET",
    url: "/v2/beers/random",
  }).as("interceptRandomBeer");
});

Cypress.Commands.add("getByTestId", (testid) => {
  return cy.get(`[data-testid="${testid}"]`);
});
