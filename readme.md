## How to run

npm i
npm start

Test integration with either
npm run cypress
npm run cypress:open

I deliberately chose to not use any app state management, eg. redux, mobx, xstate, hookstate.
## TODOS :'(
Configure a happy typescript, es6, react jest, and react-testing-library. Either with a lot of babeling or ts-jest. Only had around 0-2 hours per day during the last 5 days and it took unexpected amount of time to configure this setup (haven't done it for a while) and chose to instead work on the application. Sorry for this.

Some funny graphQL express server, with some neat cache (redis?) to store and create good suggestions when typing in the search eg.

More interesting things like expanding each card with full information using Beer component. 

Lazy load scroller / pagination.
